#!/bin/bash


if [ "$#" -ne 2 ]; then
    echo "Usage: sh ${0} SOURCE_DB_FOLDER DEST_DB_FOLDER"
    exit 1
fi

#Make sure to get absolute paths
SRCDB=`readlink -f $1`
DESDB=`readlink -f $2`

for f in $SRCDB/*
do
    fasta=`basename $f`
    CMD="cd-hit -i $f -o $DESDB/$fasta -T 0 -M 11000 -g 1 -s 0.8 -c 0.9"
    echo "Running: $CMD"
    eval $CMD
done
