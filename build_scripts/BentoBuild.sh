#!/bin/bash
VERSION=0.5

if [ "$#" -ne 3 ]; then
    echo "Usage: sh ${0} PATH_TO_BUILD_FOLDER PATH_TO_RESSOURCE_FOLDER PATH_TO_BENTOBOX"
    exit 1
fi

BUILD_DIR=`readlink -f $1`
RESOURCES=`readlink -f $2`
BENTOBOX=`readlink -f $3`
BB_BIN=${BENTOBOX}/bin/
BB_METAVELVET=${BENTOBOX}/metavelvetSL/
BB_DBs=${BENTOBOX}/DBs/
ADDPATH=$BENTOBOX


METAQC=http://www.computationalbioenergy.org/Released_Software/qc-chain/meta-qc-chain-1.0.tar.gz
VELVET=https://github.com/dzerbino/velvet.git
METAVELVET=https://github.com/hacchy/MetaVelvet.git
DWGSIM=https://github.com/nh13/DWGSIM
METAVELVET_PIPELINE=http://metavelvet.dna.bio.keio.ac.jp/srcSL/MetaVelvetSL_Pipeline.tar.gz
METAVELVET_LM=http://metavelvet.dna.bio.keio.ac.jp/srcSL/LearningModelFeatures.tar.gz
METAVELVETSL=http://metavelvet.dna.bio.keio.ac.jp/srcSL/MetaVelvetSLv1.0.tar.gz
METAPHLAN=https://bitbucket.org/nsegata/metaphlan/get/default.tar.bz2
FASTX_TOOLKIT=https://github.com/agordon/fastx_toolkit/releases/download/0.0.14/fastx_toolkit-0.0.14.tar.bz2
MGA=http://metagene.cb.k.u-tokyo.ac.jp/metagene/mga_x86_64.tar.gz
UPROC=http://uproc.gobics.de/downloads/uproc/uproc-latest.tar.gz
UPROC_MODEL=http://uproc.gobics.de/downloads/models/model.tar.gz
CDHIT=https://github.com/weizhongli/cdhit.git
KRONA=http://sourceforge.net/projects/krona/files/KronaTools%20%28Mac%2C%20Linux%29/KronaTools-2.5.tar
QUAST=http://sourceforge.net/projects/quast/files/quast-2.3.tar.gz/download
PRINSEQ=http://sourceforge.net/projects/prinseq/files/standalone/prinseq-lite-0.20.4.tar.gz/download
BB_TOOLS=https://bitbucket.org/csf_biocomp/bentobox.git

#Helper function, used to check return value of exe
check ()
{
if [ $? -ne 0 ]; then
    echo $1
    exit 1
fi   
}
echo ">>> Running BentoBuild v${VERSION} ..."
echo "Build folder: ${BUILD_DIR}"
echo "Resource folder: ${RESOURCES}"
echo "Deployment folder: ${BENTOBOX}"

mkdir -p ${BUILD_DIR}
mkdir -p ${BENTOBOX} ${BB_BIN} ${BB_METAVELVET} ${BB_DBs}

sudo apt-get update
#System utilities
sudo apt-get --assume-yes install gawk vim git p7zip tophat python-scipy python3-scipy curl screen htop 

mkdir -p ${BUILD_DIR}
check "Creating Build directory failed!"

#METAQC
echo ">>> Installing METAQC"
cd ${BUILD_DIR}
#Install hmmer, FastQC
sudo apt-get --assume-yes install hmmer fastqc libclthreads-dev libclthreads2 r-cran-ape 
#curl $METAQC -o meta-qc-chain.tar.gz
cp ${RESOURCES}/meta-qc-chain.tar.gz meta-qc-chain.tar.gz
check "Failed downloading METAQC from ${METAQC}"
mkdir meta-qc-chain
tar xf meta-qc-chain.tar.gz -C meta-qc-chain --strip-components=1
cd meta-qc-chain
tar xf meta-parallel-qc.tar.gz
cd meta-parallel-qc
make
check "Compiling meta-qc-chain failed!"
cp bin/* ${BB_BIN} 
#rm -rf meta-qc-chain

#MetaVelvetSL
echo ">>> Installing MetaVelvetSL"
cd ${BUILD_DIR}
sudo apt-get --assume-yes install zlib1g-dev libsvm-dev libsvm3 libsvm-tools samtools libgomp1 
mkdir -p ${BB_METAVELVET}
#ADDPATH=$ADDPATH:${BB_METAVELVET}

echo ">>>> Installing Velvet"
cd ${BUILD_DIR}
git clone $VELVET
check "Failed Git clone form ${VELVET}"
cd velvet
make 'OPENMP=1'
check "Compiling velvet failed!"
cp velvet* ${BB_METAVELVET}
check "Instaling velvet failed!"
cp contrib/shuffleSequences_fasta/shuffleSequences_fasta.pl ${BB_BIN}
check "Instaling shuffleSquence_fasta.pl failed!"

echo ">>>> Installing MetaVelvet"
cd ${BUILD_DIR}
sudo apt-get --assume-yes install libsvm-dev libsvm3
if false ; then
	## Using metavelvet source
	git clone $METAVELVET
	#check "Failed Git clone form ${METAVELVET}"
	cd MetaVelvet
	#Add Flag longsequences or this will cause hangups
	make DFLAGS='-D LONGSEQUENCES'
	check "Compiling MetaVelvet failed!"
else
	## Using the metaVelvetSL sources - providing meta-velvete and meta-velvetg
	curl ${METAVELVETSL} -o metavelvetSL.tar.gz
	check "Downloading MetaVelvetSL failed!"
	mkdir metaVelvetSL
	tar xf metavelvetSL.tar.gz -C metaVelvetSL --strip-components=1
	cd metaVelvetSL
	make clean && make DFLAGS='-D LONGSEQUENCES'
	check "Compiling MetaVelvetSL failed!"
fi
cp meta-velvet* ${BB_METAVELVET}

echo ">>>> Installing DWGSIM"
cd ${BUILD_DIR}
sudo apt-get--assume-yes install libncurses5-dev samtools
git clone --recursive $DWGSIM 
check "Failed Git clone form ${DWGSIM}"
cd DWGSIM
make
check "Compiling DWGSIM failed!"
cp dwgsim* ${BB_METAVELVET} 


echo ">>>> Installing MetaVelvet_Pipeline"
cd ${BUILD_DIR}
curl $METAVELVET_PIPELINE -o MetaVelvetSL_Pipeline.tar.gz
check "Failed download from ${METAVELVET_PIPELINE}"
mkdir Pipeline 
tar xf MetaVelvetSL_Pipeline.tar.gz -C Pipeline --strip-components=1
cp -r Pipeline/ReferenceGenome* ${BB_METAVELVET}


echo ">>>> Installing MetaVelvet_LearningModel"
cd ${BUILD_DIR}
curl $METAVELVET_LM -o MetaVelvetLM.tar.gz
check "Failed download from ${METAVELVET_LM}"
mkdir ${BB_METAVELVET}/LearningModelFeatures
tar xf MetaVelvetLM.tar.gz -C ${BB_METAVELVET}/LearningModelFeatures --strip-components=1

#MetaPhlan
cd ${BUILD_DIR}
echo ">>> Installing MetaPhlan"
sudo apt-get --assume-yes install bowtie2 mercurial-git ncbi-blast+ 
#hg clone https://bitbucket.org/nsegata/metaphlan/
curl $METAPHLAN -o metaphlan.tar.bz2
check "Failed download from ${METAPHLAN}"
mkdir -p ${BB_METAVELVET}/metaphlan
tar xf metaphlan.tar.bz2 -C ${BB_METAVELVET}/metaphlan --strip-components=1
check "Installing MetaPhlan failed!"

#fastx-toolkit
echo ">>> Installing fastx-toolkit"
cd ${BUILD_DIR}
sudo apt-get --assume-yes install libgtextutils-dev pkg-config
curl -L $FASTX_TOOLKIT -o fastx.tar.bz2
check "Failed download from ${FASTX_TOOLKIT}"
mkdir fastx
tar xf fastx.tar.bz2 -C fastx --strip-components=1
cd fastx
./configure && make
check "Compiling fastx_toolkit failed!"
cp src/fastq_to_fasta/fastq_to_fasta ${BB_BIN}
check "Copying fastx failed!"


echo ">>> Installing MetaGeneAnnotator"
cd ${BUILD_DIR}
curl  ${MGA} -o mga.tar.gz
check "Failed download from ${MGA}"
tar xf mga.tar.gz
cp mga_linux_ia64 ${BB_BIN}/mga

echo ">>> Installing UPROC"
cd ${BUILD_DIR}
curl -L ${UPROC} -o uproc.tar.gz
check "Failed download from ${UPROC}"
mkdir uproc
tar xf uproc.tar.gz -C uproc --strip-components=1
cd uproc
./configure && make
check "Compiling uproc failed!"
sudo make install
check "Installing uproc failed!"

echo ">>> Installing UPROC model"
sudo apt-get --assume-yes install cd-hit 
cd ${BUILD_DIR}
curl -L ${UPROC_MODEL} -o model.tar.gz
check "Failed download from ${UPROC_MODEL}"
mkdir ${BB_DBs}/uproc_model
tar xf model.tar.gz -C ${BB_DBs}/uproc_model --strip-components=1
check "Installing uproc_model failed!"

echo ">>>> Installing CD-HIT"
cd ${BUILD_DIR}
git clone $CDHIT
check "Failed Git clone form ${CDHIT}"
cd cdhit
make
check "Compiling cdHIT failed!"
cp cd-hit ${BB_BIN}

echo ">>> Installing Krona"
cd ${BUILD_DIR}
tar xf ${RESOURCES}/Krona.tar.bz2 -C ${BB_BIN}
check "Installing Krona failed!"

echo ">>> Installing MinPath"
cd ${BUILD_DIR}
tar xf ${RESOURCES}/minpath.1.2.tar.gz -C ${BB_BIN}
check "Installing MinPath failed!"

echo ">>> Installing quast"
cd ${BUILD_DIR}
curl -L ${QUAST} -o quast.tar.gz
check "Failed download from ${QUAST}"
mkdir ${BB_BIN}/quast
tar xf quast.tar.gz -C ${BB_BIN}/quast --strip-components=1
check "Installing quast failed!"

echo ">>> Installing Prinseq-lite"
cd ${BUILD_DIR}
curl -L ${PRINSEQ} -o prinseq.tar.gz
check "Failed download from ${PRINSEQ}"
mkdir prinseq 
tar xf prinseq.tar.gz -C prinseq --strip-components=1
chmod +x prinseq/prinseq-lite.pl
cp prinseq/prinseq-lite.pl ${BB_BIN}
check "Installing prinseq failed!"

echo ">>> Installing DataBases"
cd ${BENTOBOX}
7zr x ${RESOURCES}/DBs.7z -o${BENTOBOX}
check "Installing DataBases failed!"

echo ">>> Installing Bento-Box-Tools"
cd ${BUILD_DIR}
git clone ${BB_TOOLS} 
check "Git failed for ${BB_TOOLS}"
cp -r bentobox/scripts/* ${BB_BIN}
check "Instaling Bento-Box-Tools failed!"

ADDPATH=$ADDPATH:${BB_BIN}
ADDPATH=$ADDPATH:${BB_METAVELVET}
ADDPATH=$ADDPATH:${BB_METAVELVET}/metaphlan
echo "export PATH=${ADDPATH}:\$PATH" > ${BENTOBOX}/env.sh

echo "Add to you path the following"
echo "export \$PATH=${ADDPATH}:\$PATH"
