import sys
import re
import os.path

def usage():
    print('Split a reference database in fasta format into multiple smaller files, by genus. \
           Assumes that the fasta file is sorted by genus.')
    print('Usage: python3 split_by_genus.py SeedDB.fa')

def main():
    try:
        db_file = sys.argv[1]

    except Exception as e:
        print('Error! Invalid input')
        usage()
        sys.exit(2)

    try:
        with open(db_file, encoding = "ISO-8859-1") as file:
            id_line = re.compile('(\S*)\s(.*)\s+\[([^\s\[\]]+)\s.*]')
            curr_genus = None
            genus_file = None
            for line in file:
                if line.startswith('>'):
                    m = id_line.match(line)
                    if m is not None:
                        genus = m.group(3)
                    else:
                        raise AttributeError("Can't extract genus from id (%s)" % line)

                    if genus != curr_genus:
                        if genus_file is not None:
                            genus_file.close()
                        curr_genus = genus
                        genus_file = open(genus+".fa", 'a')
                        print('Creating new genus file %s' % (genus+'.fa'))

                genus_file.write(line)

    except FileNotFoundError as e:
        print('Reading seed db fasta file failed!')
        raise(e)
    except Exception as e:
        print('Unknown Error!')
        raise(e)

if __name__ == "__main__":
        main()
