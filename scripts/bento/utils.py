'''
Utility functions for the Bento-box scripts

@author:     Serrano Drozdowskyj, Pedro - pedro.serranno@csf.ac.at

@copyright:  2015 BioComp/CSF. All rights reserved.

@license:    TODO
'''

import csv

def dictlist_to_csv(dict_data, file, columns, delimiter=','):
    ''' Write a list of dictionaries to a CSV file '''
    writer = csv.DictWriter(file, fieldnames=columns, delimiter=delimiter)
    writer.writeheader()
    for data in dict_data:
        writer.writerow(data)
