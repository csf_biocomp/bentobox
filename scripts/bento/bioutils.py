'''
Biology related utility functions for the Bento-box scripts

@author:     Serrano Drozdowskyj, Pedro - pedro.serranno@csf.ac.at

@copyright:  2015 BioComp/CSF. All rights reserved.

@license:    TODO
'''

# The Bacterial, Archaeal and Plant Plastid Code: SG11
BAP_aminoacids = {
   "TTT":"F", "TTC":"F", "TTA":"L", "TTG":"L",
   "TCT":"S", "TCC":"S", "TCA":"S", "TCG":"S",
   "TAT":"Y", "TAC":"Y", "TAA":'*', "TAG":'*',
   "TGT":"C", "TGC":"C", "TGA":'*', "TGG":"W",
   "CTT":"L", "CTC":"L", "CTA":"L", "CTG":"L",
   "CCT":"P", "CCC":"P", "CCA":"P", "CCG":"P",
   "CAT":"H", "CAC":"H", "CAA":"Q", "CAG":"Q",
   "CGT":"R", "CGC":"R", "CGA":"R", "CGG":"R",
   "ATT":"I", "ATC":"I", "ATA":"I", "ATG":"M",
   "ACT":"T", "ACC":"T", "ACA":"T", "ACG":"T",
   "AAT":"N", "AAC":"N", "AAA":"K", "AAG":"K",
   "AGT":"S", "AGC":"S", "AGA":"R", "AGG":"R",
   "GTT":"V", "GTC":"V", "GTA":"V", "GTG":"V",
   "GCT":"A", "GCC":"A", "GCA":"A", "GCG":"A",
   "GAT":"D", "GAC":"D", "GAA":"E", "GAG":"E",
   "GGT":"G", "GGC":"G", "GGA":"G", "GGG":"G",
}


def fasta_to_dict(file):
    '''
    Returns a dictionary of sequences from the contents of a fasta file
    '''
    d = {}
    for line in file:
        if not line.strip():
            continue

        if line.startswith('>'):
            name = line[1:].strip()
            d[name] = ''
            continue
        d[name] += line.strip()

    return d


FASTA_width = 80 # Width of a FASTA line
def dict_to_fasta(d, file, wrap_lines=True):
    '''
    Saves a dictionary of sequences to a fasta file
    '''
    for id, sequence in d.items():
        print('>%s' % id, file=file)
        if wrap_lines:
            for i in range(0, len(sequence), FASTA_width):
                print(sequence[i:i+FASTA_width], file=file)
        else:
            print(sequence, file=file)



def get_codons(seq):
    '''
    Iterator to parse sequences in chunks of 3 base pairs. Incomplete (i.e. if
    the length is not a multiple of 3) are note yielded.

    >>> [c for c in get_codons('AAACCCGGG')]
    ['AAA', 'CCC', 'GGG']
    >>> [c for c in get_codons('AAACCCG')]
    ['AAA', 'CCC']
    >>> [c for c in get_codons('AAACCCGGGTT')]
    ['AAA', 'CCC', 'GGG']
    '''
    for i in range(0, 3*(len(seq)//3), 3):
        yield seq[i:i+3]


def translate(seq, codon_table, frame=0):
    '''
    Translate a sequence into a protein, given a codon table

    >>> translate('ATGGCCATGGCGCCCAGAACTGAGATCAATAGTACCCGTATTAACGGGTGA', BAP_aminoacids)
    'MAMAPRTEINSTRING*'
    >>> translate('ATGGCCATGGCGCCCAGAACTGAGATCAATAGTACCCGTATTAACGGGT', BAP_aminoacids)
    'MAMAPRTEINSTRING'
    '''
    if frame < 0 or frame > 2:
        raise AttributeError("Translation frame must be a value between 0 and 2 (%d given)" % frame)

    prot = ''
    for codon in get_codons(seq.upper()[frame:]):
        prot += 'X' if 'N' in codon else codon_table[codon]

    return prot

IUAPAC_Complement = {
    'A': 'T', 'B': 'V', 'C': 'G', 'D': 'H',
    'G': 'C', 'H': 'D', 'K': 'M', 'M': 'K',
    'N': 'N', 'R': 'Y', 'S': 'S', 'T': 'A',
    'V': 'B', 'W': 'W', 'Y': 'R',

    'a': 't', 'b': 'v', 'c': 'g', 'd': 'h',
    'g': 'c', 'h': 'd', 'k': 'm', 'm': 'k',
    'n': 'n', 'r': 'y', 's': 's', 't': 'a',
    'v': 'b', 'w': 'w', 'y': 'r'
}
def rev_complement(dna):
    '''
    Reverse complement a DNA sequence

    >>> rev_complement('AAAACCCGGT')
    'ACCGGGTTTT'
    >>> rev_complement('aaaaCCCggTNn')
    'nNAccGGGtttt'
    '''
    try:
        return ''.join([IUAPAC_Complement[base] for base in dna][::-1])

    except KeyError as exc:
        raise AttributeError("Sequence is not a valid IUAPAC DNA string" +
                            "(invalid nucleotide: {})".format(e)
        )
