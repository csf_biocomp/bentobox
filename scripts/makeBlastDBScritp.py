import sys
import getopt
import re
import os.path

def usage():
    #print('Read Taxon information and generate a set of reference samples from similar species')
    print('Usage: python3 makeBlastDBScript.py TaxonFile BlastDB_Fragments OutputScript')
    print('e.g: python3 ReadTaxon.py /srv/bentobox/tmp/ResultProfile /srv/bentobox/DBs/BlastDB /srv/bentobox/tmp/makeBlastDB.sh')

def main():
    try:
        taxonfile, refDir, bashfile =  sys.argv[1:]
    
    except Exception as e:
        print('Error! Invalid input')
        usage()
        sys.exit(2)

    try:
        with open(taxonfile) as tf:
            ll = tf.readlines()

        #Filter all Entires, remaining only species, ans split them into genus, species and VALUE
        pattern = re.compile('.+s__(\w+)_(\w+)\t*\s*(\d+.\d+)\n$')
        species=[]
        for l in ll:
            m = pattern.match(l) 
            if m is not None:
                g, s, t1 = m.groups()                
                species.append([g, s[0].upper() + s[1:], float(t1)])

        #print(species)
        #For each Entry Generate a call to dwgSim
        with open(bashfile, mode='w') as bs:
            bs.write('#!/bin/sh\n')
            bs.write('mkdir -p $2\n')
            #bs.write('cd %s' % (outDir))
            fragments = ''
            i = 0
            for g ,s ,m in species:
                fragments += format('%s/%s.fa ' % (refDir, g))

            bs.write(format('cat %s > %s\n' % (fragments, '$1')))
            bs.write(format('makeblastdb -in $1 -input_type fasta -dbtype prot -out $2\n'))

    except FileNotFoundError as e:
        print('Reading Taxon file failed!')
        raise(e)
    except Exception as e:
        print('Unknown Error!')
        raise(e)

if __name__ == "__main__":
        main()
