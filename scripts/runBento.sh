#!/bin/bash
VERSION=0.4

if [ "$#" -ne 4 ]; then
    echo "Usage: sh ${0} PATH_TO_BENTOBOX PATH_TO_WORKING_DIRECTORY INPUT_DATA_FOLDER %CORES"
    exit 1
fi

#Pipeline Directory
PL=`readlink -f $1`
WD=`readlink -f $2`
IDATA=`readlink -f $3`
NCORES=$4
BDIR=${PL}/bin
IDIR=${WD}/input
DBDIR=${PL}/DBs
METAVELVETSL=${PL}/metavelvetSL
METAPHLAN=${METAVELVETSL}/metaphlan


#Subdirectories in WorkingDir
AD=${WD}/Assembly
LMD=${WD}/SimDataLearningModel

#Increase application stack to 65MB in order to solve issues with meta-velvet
ulimit -c 65536
####################### HELPER FUNCTIONS ######################################

#Helper function, used to check return value of exe
check ()
{
    lastReturn=$?
    trackFileChanges $1
    if [ $lastReturn -ne 0 ]; then
        echo "Step ${1} failed!"
        exit 1
    fi   
}

startTrackFileChanges()
{
    mkdir -p $2
    #ls -Rhl $1 --hide=`basename $2` > $2/latest.txt
    find .  -path `basename $2` -prune -o -name '*' -print > $2/latest.txt
    cp $2/latest.txt $2/InitialContent.txt
    eval MONITOR_DIR=$1; STATS_DIR=$2
}

trackFileChanges ()
{
    STEP_NAME=$1
    #ls -Rhl ${MONITOR_DIR} --hide=`basename ${STATS_DIR}` > ${STATS_DIR}/now.txt
    find .  -path ${MONITOR_DIR} -prune -o -name '*' -print > ${STATS_DIR}/now.txt
    diff ${STATS_DIR}/latest.txt ${STATS_DIR}/now.txt > ${STATS_DIR}/${STEP_NAME}.diff
    mv ${STATS_DIR}/now.txt ${STATS_DIR}/latest.txt
}


####################### HELPER FUNCTIONS ######################################



echo "########################################################################"
echo ">>> Running MetaVelvet Pipeline v${VERSION} ..."
echo "########################################################################"

startTrackFileChanges ${WD} ${WD}/BentoStats
echo "Monitoring ${MONITOR_DIR} and writing stats to ${STATS_DIR}"

echo "########################################################################"
echo "#######################  Preprocessing Input  ##########################"
echo "########################################################################"

echo "########################################################################"
#Prepare input data for metaphlan
echo -n ">>> Using input from "; md5sum ${IDATA}
mkdir -p ${IDIR}

cp ${IDATA} ${IDIR}/input.bam
cp ${IDATA} ${IDIR}/clean_input.fasta

mkdir -p ${WD}/results

echo "########################################################################"
cd ${WD}
STEP="01_fastqc"
CMD="fastqc ${IDIR}/input.bam -o ${IDIR}"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"


echo "########################################################################"
cd ${WD}
STEP="02_BAM2FASTQ"
CMD="bam2fastx -PAN -o ${IDIR}/input.fastq ${IDATA}"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
cd ${WD}
STEP="03_prinseq"
CMD="prinseq-lite.pl -fastq ${IDIR}/input.1.fastq -fastq2 ${IDIR}/input.2.fastq -out_format 1 -min_qual_mean 20 -out_good ${IDIR}/clean_input"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

#TODO: maybe exchange shuffleSequence for its C version from velvet/contrib/shuffleSequence ....
echo "########################################################################"
cd ${WD}
STEP="04_shuffleSequences_fasta"
CMD="shuffleSequences_fasta.pl ${IDIR}/clean_input_1.fasta ${IDIR}/clean_input_2.fasta ${IDIR}/clean_input.fasta"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
echo "#######################  Running MetavelvetSL  #########################"
echo "########################################################################"

echo "########################################################################"
echo ">>> Executing Metaphlan on ${IDATA}/clean_input.fasta ..."
STEP="05_MetaPhlan"
cd ${WD}
CMD="cat ${IDIR}/clean_input.fasta | metaphlan.py --bowtie2db ${METAPHLAN}/bowtie2db/mpa --bt2_ps very-sensitive --input_type multifasta --nproc ${NCORES} > ${WD}/ResultProfile.txt"
echo "Running: ${CMD}"
eval ${CMD}
check $STEP
echo "Finished Metaphlan!"
echo "########################################################################"

#Simulated input data generation
echo "########################################################################"
echo ">>> Running simulated input data Generator Script ..."
cd ${WD}
STEP="06_ReadTaxon"
CMD="ReadTaxon.py ${WD}/ResultProfile.txt ${METAVELVETSL}/ReferenceGenome 30 ${WD}/simData ${WD}/GenData.sh ${NCORES}"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP 
echo "########################################################################"

echo "########################################################################"
echo ">>> Generating simulated Input ..."
STEP="07_GenData"
CMD="sh ${WD}/GenData.sh ${WD}/AllReads.fasta ${WD}/ReferenceGenomes.fasta"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "Finished data simulation!"
echo "########################################################################"

echo "########################################################################"
echo ">>> Generating simulated  Learning Model..."
STEP="08_velveth"
mkdir -p ${LMD}
export OMP_NUM_THREADS=${NCORES}
CMD="velveth ${LMD} 31 -shortPaired -fasta ${WD}/AllReads.fasta"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
STEP="09_velvetg"
cd ${WD}
CMD="velvetg ${LMD} -ins_length 500 -read_trkg yes -exp_cov auto"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
STEP="10_meta-velvete"
cd ${WD}
CMD="meta-velvete ${LMD} -ins_length 500"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
echo ">>> Evaluating LearningModel ..."
STEP="11_LearningModelFeatures"
cd ${WD}
cd ${METAVELVETSL}/LearningModelFeatures/BLAST_map
check "Change_to_LearningModel_Dir"
CMD="perl eval.pl -i ${LMD}/meta-velvetg.subgraph__ChimeraNodeCandidates -n ${LMD} -d ${WD}/ReferenceGenomes.fasta -p ${LMD} -P ${NCORES}"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
STEP="12_FeatureExtract"
cd ${WD}
CMD="perl ${METAVELVETSL}/LearningModelFeatures/FeatureExtract.perl ${LMD}/meta-velvetg.subgraph__TitleChimeraNodeCandidates ${WD}/SimDataLearningModel.long-scafs.blast ${LMD}/Features3Class ${LMD}/ChimeraTrue ${LMD}/ChimeraNodeName" 
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
STEP="13_svm-easy"
cd ${WD}
cp ${LMD}/Features3Class ${LMD}/LearningModel
CMD="svm-easy ${LMD}/LearningModel"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
cd ${WD}
STEP="14_velveth"
export OMP_NUM_THREADS=${NCORES}
CMD="velveth ${AD} 31 -fasta -shortPaired ${IDIR}/clean_input.fasta"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
STEP="14_velvetg"
cd ${WD}
CMD="velvetg ${AD} -read_trkg yes -exp_cov auto -ins_length 260"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
STEP="15_meta-velvete"
cd ${WD}
CMD="meta-velvete ${AD} -ins_length 260"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
STEP="16_FeatureExtractPredict"
cd ${WD}
echo "Running FeatureExtractPredict ..."
CMD="perl ${METAVELVETSL}/LearningModelFeatures/FeatureExtractPredict.perl ${AD}/meta-velvetg.subgraph__TitleChimeraNodeCandidates ${AD}/Features ${AD}/Features3Class ${AD}/ChimeraNodeName"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
STEP="17_svm-scale"
cd ${WD}
CMD="svm-scale -r ${WD}/LearningModel.range ${AD}/Features3Class > ${AD}/Features3Class.scale"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
STEP="18_svm-predict"
eval ${CMD}
cd ${WD}
CMD="svm-predict ${AD}/Features3Class.scale ${WD}/LearningModel.model ${AD}/ClassificationPredict"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
STEP="19_meta-velvetg"
cd ${WD}
CMD="meta-velvetg ${AD} -ins_length 260"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
STEP="20_mga"
cd ${WD}
CMD="mga ${AD}/meta-velvetg.contigsSL.fa -m > PredictedGenes.txt"
echo "Running: ${CMD}"
eval ${CMD}
check $STEP
echo "########################################################################"

echo "########################################################################"
STEP="21_extract_seq"
cd ${WD}
mkdir ContigSeqs
CMD="python3 ${BDIR}/extract_seqs.py ${WD}/PredictedGenes.txt ${AD}/meta-velvetg.contigsSL.fa ContigSeqs"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
STEP="21_makeBlastDBScript.py"
cd ${WD}
CMD="python3 ${BDIR}/makeBlastDBScritp.py ${WD}/ResultProfile.txt ${DBDIR}/BlastDB ${WD}/makeBlastDB.sh"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"


echo "########################################################################"
STEP="22_makeblastdb"
cd ${WD}
mkdir -p ${WD}/DBs/BlastDB
CMD="sh ${WD}/makeBlastDB.sh ${WD}/DBs/BlastDB/FullDB.fasta ${WD}/DBs/BlastDB"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
STEP="23_blastp"
cd ${WD}
CMD="blastp -db ${WD}/DBs/BlastDB -out AlignedProteins.txt -evalue 0.000001 -num_threads ${NCORES} -num_alignments 10 -outfmt '7 qseqid qlen salltitles pident evalue' -query ContigSeqs/proteins.fasta"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
STEP="24_BlastTopHit"
cd ${WD}
CMD="BlastTopHit.sh ${WD}/AlignedProteins.txt > ${WD}/results/TopAlignedProteins.txt"
echo "Running: ${CMD}"
eval "time " ${CMD}
check $STEP
echo "########################################################################"


echo "########################################################################"
STEP="25_metaphlan2Krona"
cd ${WD}
mkdir Krona
CMD="$METAPHLAN/conversion_scripts/metaphlan2krona.py -p ${WD}/ResultProfile.txt -k ${WD}/Krona/Krona.txt"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
STEP="26_Krona"
cd ${BDIR}/Krona
CMD="perl ImportText.pl -a -o ${WD}/results/Krona_protocol.html ${WD}/Krona/Krona.txt"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
STEP="27_uproc-prot_KEGG"
cd ${WD}
CMD="uproc-prot -t ${NCORES} -p -o ${WD}/KEGGProteins.txt ${DBDIR}/KEGG/ ${DBDIR}/uproc_model/ ${WD}/ContigSeqs/proteins.fasta"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
STEP="28_gen_proteinKEGGIDs"
cd ${WD}
CMD="cut -d',' -f2,4 --output-delimiter=$'\t' ${WD}/KEGGProteins.txt > ${WD}/ProteinsKEGGIDs.ko"
echo "Running: ${CMD}"
eval "time " ${CMD}
check $STEP
echo "########################################################################"

echo "########################################################################"
STEP="29_uproc-prot_PFAM"
cd ${WD}
CMD="uproc-prot -t ${NCORES} -p -o ${WD}/PFAMProteins.txt ${DBDIR}/PFAM/ ${DBDIR}/uproc_model/ ${WD}/ContigSeqs/proteins.fasta"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
STEP="30_pfam_results.py"
cd ${WD}
CMD="pfam_results.py PFAMProteins.txt ${DBDIR}/PFAM_families.dat ${WD}/results/pfam_results.txt"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
STEP="31_Quast"
mkdir -p ${WD}/results/quast
cd ${BDIR}/quast
CMD="python quast.py ${AD}/meta-velvetg.contigsSL.fa -o ${WD}/results/quast"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
STEP="32_MinPath"
mkdir -p ${WD}/MinPath
export MinPath=.
cd ${BDIR}/MinPath
CMD="./MinPath1.2.py -ko ${WD}/ProteinsKEGGIDs.ko -report ${WD}/MinPath/report.txt -details ${WD}/MinPath/details.txt"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
STEP="33_kegg_results.py"
cd ${WD}
CMD="kegg_results.py ${WD}/MinPath/details.txt ${WD}/KEGGProteins.txt ${DBDIR}/KEGG_pathway_groups.csv ${WD}/results/KEGGResults.txt"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "########################################################################"
STEP="34_populating_result"
echo "Populating result folder ..."
CMD="cp ${WD}/ContigSeqs/genes.fasta ${WD}/ContigSeqs/proteins.fasta ${AD}/meta-velvetg.contigsSL.fa ${WD}/results"
echo "Running: ${CMD}"
eval "time " ${CMD} " > ${STATS_DIR}/$STEP.log 2>&1"
check $STEP
echo "########################################################################"

echo "Finish BentoRun!"
