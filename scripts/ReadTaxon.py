#!/usr/bin/env python3
import sys
import getopt
import re
import os.path

def usage():
    #print('Read Taxon information and generate a set of reference samples from similar species')
    print('Usage: python3 ReadTaxon.py TaxonFile ReferenceGenomeDirectory multiplyer OutputDirectory outputScript nThreads')
    print('e.g: python3 ReadTaxon.py /srv/bentobox/tmp/ResultProfile /srv/bentobox/ReferenceGenome 30 /srv/bentobox/tmp/simData /srv/bentobox/tmp/GenData.sh 4')

def main():
    try:
        taxonfile, refDir, M, outDir, bashfile, nThreads = sys.argv[1:]
        M = float(M)
        nThreads = int(nThreads)

    except Exception as e:
        print('Error! Invalid input')
        usage()
        sys.exit(2)

    try:
        with open(taxonfile) as tf:
            ll = tf.readlines()

        #Filter all Entires, remaining only species, ans split them into genus, species and VALUE
        pattern = re.compile('.+s__(\w+)_(\w+)\t*\s*(\d+.\d+)\n$')
        species=[]
        for l in ll:
            #if 's__' in l:
            m = pattern.match(l) 
            if m is not None:
                g, s, t1 = m.groups()                
                species.append([g, s[0].upper() + s[1:], float(t1)])

        #print(species)
        #For each Entry Generate a call to dwgSim
        with open(bashfile, mode='w') as bs:
            bs.write('#!/bin/sh\n')
            bs.write('mkdir -p %s\n' % (outDir))
            #bs.write('cd %s' % (outDir))
            refFiles = ''
            for i, (g ,s ,m) in enumerate(species):
                gF = refDir + '/' + g + s + '.fa'
                if os.path.isfile(gF):
                    bs.write(format('(dwgsim -1 80 -2 80 -e 0.0.1 -E 0.01 -C %f %s/%s.fa %s/%s && fastq_to_fasta -i %s/%s.bfast.fastq -o %s/%s.fasta) &\n' % (m*M, refDir, g+s, outDir, g+s, outDir, g+s, outDir, g+s)))
                    if(i % nThreads == nThreads-1):
                        bs.write('wait\n')
                    refFiles += format('%s/%s.fa ' % (refDir, g + s))
                else:
                    print('Skipping missing reference %s' % gF)

            bs.write('wait\n')
            bs.write(format('cat %s/*.fasta > %s\n' % (outDir, '$1')))
            bs.write(format('cat %s > %s\n' % (refFiles, '$2')))

    except FileNotFoundError as e:
        print('Reading Taxon file failed!')
        raise(e)
    except Exception as e:
        print('Unknown Error!')
        raise(e)

if __name__ == "__main__":
        main()
