#!/bin/bash

#
# Extracts the topmost hit for each of the proteins in the blast results
# Proteins with no hits
#
# This command expects a specific format from blast out. Here's an example:
# blastp -db seed.fa -out blast_custom.out -evalue 0.000001 -num_threads 2 -num_alignments 10 -outfmt "7 qseqid qlen salltitles pident evalue" -query all_proteins_OUTPUT.fasta
#
# Requires gawk!
#

if [ -z "$1" ]
  then
    echo "Please specify the blast results file to process"
    exit 1
fi

# 1. Add a "No hits" for each protein, which will be at the bottom when sorting
sed 's/^# Query: \(.*\)/\1	No hits found/' $1 | \
# 2. remove all comments
sed '/^#/ d' | \
# 3. sort by protein id and score (this is necessary because what we put in in step 1. could be in other ways)
sort -t$'\t' -k1,1 -k5,5gr  | \
# 4. extract the top hit and format protein info
awk -F $'\t' -v OFS='\t' '
BEGIN {print "Protein", "Length", "Gene Id", "Gene Name", "Species", "Scrore", "e-value"}
NF > 0 && !_[$1]++ {
  $3 = gensub(/(\S*)\s(.*)\[([^\[\]]*)]$/, "\\1	\\2	\\3", 1, $3)
  print
}
'
