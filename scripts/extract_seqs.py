#!/usr/bin/env python3

'''
Extract Sequences

Given the results of the gene prediction step of the pipeline,
produce the fasta files containing the DNA and Protein sequences
for those genes

@author:     Serrano Drozdowskyj, Pedro - pedro.serranno@csf.ac.at

@copyright:  2015 BioComp/CSF. All rights reserved.

@license:    TODO
'''

import os
import logging
import argparse
from collections import OrderedDict

from bento.utils import dictlist_to_csv
from bento.bioutils import *
from bento.commands import BentoCommand, arg_is_valid_directory, arg_int_in_range

min_seq_lenght = 300
def extract_sequences(predicted_genes_file, contigs_file, results_dir, min_length=min_seq_lenght):
    '''
    Given a list of predicted genes (in custom format) and a list of
    contig sequences in fasta format, generate the fasta files with
    the dna and protein sequences for the predicted genes. Predicted genes
    that are shorter than the specified minimum length will be skipped.
    '''
    predicted_genes = parse_predicted_genes(predicted_genes_file)
    logging.debug('Loaded %d predicted genes' % len(predicted_genes))

    contig_seqs = fasta_to_dict(contigs_file)
    logging.debug('Loaded %d contig sequences' % len(contig_seqs))

    # TODO: do we care about the order?
    gene_seqs = OrderedDict()
    prot_seqs = OrderedDict()
    for i, gene in enumerate(predicted_genes):
        # NOTE: gene ids are assigned sequentially and not taken from the
        # predicted genes file
        gene_id = 'gene_%d' % (i + 1)
        protein_id = 'protein_%d' % (i + 1)

        # TODO: check for 0 based calculations?
        # TODO: minlenght check
        sequence = contig_seqs[gene['contig']][gene['start'] - 1:gene['end']]
        if len(sequence) >= min_length:
            if (gene['strand'] == '-'):
                sequence = rev_complement(sequence)

            gene_seqs[gene_id] = sequence
            prot_seqs[protein_id] = translate(sequence, BAP_aminoacids, gene['frame'])
        else:
            logging.debug('Skipped gene %s for having a short sequence (%dbp)' % (gene_id, len(sequence)))

    # Data output to files
    # TODO: export genes to CSV? split by phyla?
    with open(os.path.join(results_dir, 'predicted_genes.tsv'), 'w') as file:
        dictlist_to_csv(predicted_genes, file, predicted_gene_props, '\t')
        logging.info('Saved predicted genes table to %s' % file.name)

    with open(os.path.join(results_dir, 'genes.fasta'), 'w') as file:
        dict_to_fasta(gene_seqs, file)
        logging.info('Saved DNA sequences to %s' % file.name)

    with open(os.path.join(results_dir, 'proteins.fasta'), 'w') as file:
        dict_to_fasta(prot_seqs, file)
        logging.info('Saved protein sequences to %s' % file.name)


predicted_gene_props = ['contig', 'phyla', 'gene', 'start', 'end', 'strand', 'frame',
                        'compl', 'g_score', 'model', 'rbs_start', 'rbs_end', 'rbs_score']

def parse_predicted_genes(file):
    '''
    Parse the file containing the predicted genes and return them
    as a list of dicts
    '''
    genes = []
    for line in file:
        line = line.strip()
        if line.startswith('#'):
            contig = line.split(' ')[1]
            next(file) # skip gc
            phyla = next(file).strip().split(' ')[2]
            continue
        else:
            gene = dict(zip(predicted_gene_props, [contig, phyla] + line.split('\t')))
            # TODO: Value type transformations
            for field in ['start', 'end', 'frame']:
                gene[field] = int(gene[field])
            genes.append(gene)

    return genes


class ExtractSeqs(BentoCommand):
    short_description = "Extract predicted gene sequences"

    @classmethod
    def _arg_parser(cls):
        parser = argparse.ArgumentParser(description='Extract the DNA and protein sequences for predicted genes')
        parser.add_argument('predicted_genes', type=argparse.FileType('r'),
                            help='A text file containining the results of the gene prediction')
        parser.add_argument('contigs_file', type=argparse.FileType('r'),
                            help='A fasta file containining the sequences for the contigs')
        parser.add_argument('outfolder', type=lambda x: arg_is_valid_directory(parser, x),
                            help='Path to directory where the result fasta files will be stored')
        parser.add_argument('-ml', '--minlength', type=lambda x: arg_int_in_range(parser, x), default=min_seq_lenght,
                            help='Minimumn sequence length, in basepairs (shorter sequences are not included in the output')

        return parser

    @classmethod
    def _main(cls, args):
        extract_sequences(args.predicted_genes, args.contigs_file, args.outfolder, args.minlength)


if __name__ == '__main__':
    ExtractSeqs.run()
