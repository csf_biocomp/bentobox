#!/usr/bin/env python3

'''
PFAM results

Create a table from PFAM analysis results with Domain/Family info for each protein.

@author:     Serrano Drozdowskyj, Pedro - pedro.serranno@csf.ac.at

@copyright:  2015 BioComp/CSF. All rights reserved.

@license:    TODO
'''

import os
import logging
import argparse
import csv
import re

from bento.utils import dictlist_to_csv
from bento.commands import BentoCommand, arg_is_valid_directory


def pfam_results(uproc_file, pfam_families_file, outfile):
    '''
    Given the the UProC PFAM analyisis results and the PFam families annotations,
    generate a summary table adding the annotations to the results
    '''
    pfam_families = parse_pfam_families(pfam_families_file)
    proteins = parse_uproc_pfam_results(uproc_file)

    # Add PFAM family information
    # TODO: is there some filtering going on?
    for protein in proteins:
        protein.update(pfam_families[protein['PFAM_Id']])

    # Data output to files
    fields = ['Nr', 'Protein', 'Length', 'PFAM_Id', 'Score', 'GeneFunction', 'EntryType']
    dictlist_to_csv(proteins, outfile, fields, '\t')
    logging.info('Saved PFAM summary table to to %s' % outfile.name)


def parse_pfam_families(file):
    '''
    Parse the file containing the PFam families
    '''
    families = {}

    for line in file:
        if line.startswith('#=GF AC'):
            pfam_id = re.split('\s+|\.', line)[2]
            pfam_gf = re.split('DE\s+', next(file))[1]
            next(file) # Skip GA
            pfam_type = re.split('TP\s+', next(file))[1]
            families[pfam_id] = dict(zip(['GeneFunction', 'EntryType'], [pfam_gf.strip(), pfam_type.strip()]))

    return families


def parse_uproc_pfam_results(file):
    '''
    Parse the file containing the UProC results into a list of dicts
    '''
    reader = csv.DictReader(file, fieldnames=['Nr', 'Protein', 'Length', 'PFAM_Id', 'Score'])
    return [row for row in reader]


class PfamResults(BentoCommand):
    short_description = "Summarize Pfam analysis results"

    @classmethod
    def _arg_parser(cls):
        parser = argparse.ArgumentParser(description='Summarize the PFAM analysis results')
        parser.add_argument('uproc_results', type=argparse.FileType('r'),
                            help='A csv file containing the output from the UProC PFAM analysis')
        parser.add_argument('pfam_families', type=argparse.FileType('r'),
                            help='A text file containing the Pfam families information')
        parser.add_argument('outfile', type=argparse.FileType('w'),
                            help='Path to file where the resulting summary table will be stored')

        return parser

    @classmethod
    def _main(cls, args):
        pfam_results(args.uproc_results, args.pfam_families, args.outfile)


if __name__ == '__main__':
    PfamResults.run()
