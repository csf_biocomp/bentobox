#!/usr/bin/env python3

'''
KEGG results

Generate the KEGG summary table for the user, that summarizes the
results of the UProC and the MinPath tools in readable format

@author:     Serrano Drozdowskyj, Pedro - pedro.serranno@csf.ac.at

@copyright:  2015 BioComp/CSF. All rights reserved.

@license:    TODO
'''

import os
import logging
import argparse
import re
import csv

from bento.utils import dictlist_to_csv
from bento.commands import BentoCommand, arg_is_valid_directory

def merge_dicts(a, b):
    ''' Merge two dicts '''
    d = a.copy()
    d.update(b)
    return d

def kegg_results(minpath_file, uproc_file, kegg_groups_file, outfile):
    '''
    Given the detailed results of MinPath analysis, the results of the UProC
    analysis and the list of kegg pathway groups, generate a summary table in
    readable format
    '''
    pathway_groups = parse_kegg_groups(kegg_groups_file)
    proteins = parse_uproc_results(uproc_file)
    kegg_pathways = parse_minpath_results(minpath_file)


    # Filter and add pathway group
    results = []
    for protein in proteins:
        if protein['KEGG_Id'] in kegg_pathways:
            for pathway_info in kegg_pathways[ protein['KEGG_Id'] ]:
                result = merge_dicts(protein, pathway_info)
                result['PathwayGroup'] =  pathway_groups.get(result['PathwayId'], "(no metabolic group available)" )
                results.append(result)

    # Data output to files
    fields = ['Nr', 'Protein', 'Length', 'KEGG_Id', 'KEGG_Name', 'Score', 'PathwayId', 'PathwayName', 'PathwayGroup']
    dictlist_to_csv(results, outfile, fields, '\t')
    logging.info('Saved KEGG summary table to to %s' % outfile.name)


def parse_kegg_groups(file):
    '''
    Parse the file containing the KEGG pathways groups into a dict
    '''
    reader = csv.DictReader(file, fieldnames=['PathwayId', 'Group'], delimiter='\t')

    pathways = {}
    for row in reader:
        pathways[row['PathwayId']] = row['Group']

    return pathways


def parse_uproc_results(file):
    '''
    Parse the file containing the UProC results into a list of dicts
    '''
    reader = csv.DictReader(file, fieldnames=['Nr', 'Protein', 'Length', 'KEGG_Id', 'Score'])
    # TODO: filter by similarity score?
    return [row for row in reader]


def parse_minpath_results(file):
    '''
    Parse the file containing the minpath detailed results and return them
    as a dict with KEGG identifier as the key
    '''
    pathway_re = re.compile('^path (\d+) .*# (.*)$')
    kegg_re = re.compile('^\s+(K\d+).*# (.*)$')

    kegg_ids = {}
    for line in file:
        pathway = pathway_re.match(line)
        kegg = kegg_re.match(line)
        if pathway:
            pathway_id, patway_name = pathway.groups()
        elif kegg:
            kegg_id, kegg_name = kegg.groups()

            kegg_ids.setdefault(kegg_id, [])

            kegg_ids[kegg_id].append( dict(zip(['KEGG_Name', 'PathwayId', 'PathwayName'],
                                      [kegg_name, pathway_id, patway_name])))
        elif line.strip(): # Not empty line
            print("Line: " + line)
            raise ValueError("Provided file doesn't seem to be a valid MinPath detailed results file (%s)" % file.name)

    return kegg_ids


class KeggResults(BentoCommand):
    short_description = "Summarize KEGG analysis results"

    @classmethod
    def _arg_parser(cls):
        parser = argparse.ArgumentParser(description='Summarize the KEGG analysis results')
        parser.add_argument('minpath_results', type=argparse.FileType('r'),
                            help='A text file containining the detailed results of the MinPath analysis')
        parser.add_argument('uproc_results', type=argparse.FileType('r'),
                            help='A csv file containing the output from the UProC analysis')
        parser.add_argument('kegg_groups', type=argparse.FileType('r'),
                            help='A tsv file containing the KEGG groups information')
        parser.add_argument('outfile', type=argparse.FileType('w'),
                            help='Path to file where the resulting summary table will be stored')

        return parser

    @classmethod
    def _main(cls, args):
        kegg_results(args.minpath_results, args.uproc_results, args.kegg_groups, args.outfile)


if __name__ == '__main__':
    KeggResults.run()
