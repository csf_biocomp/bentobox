'''
Tests for the bento-box command line scriptts

@author:     Serrano Drozdowskyj, Pedro - pedro.serranno@csf.ac.at

@copyright:  2015 BioComp/CSF. All rights reserved.

@license:    TODO
'''

import os

FIXTURES_DIR = os.path.join(os.path.dirname(__file__), 'fixtures')
def fixture_file(filename):
    return os.path.join(FIXTURES_DIR, filename)

def test_extract_sequences():
    '''
    Test the extract sequences script
    '''
    genes   = open(fixture_file('predicted_genes.txt'), 'r')
    contigs = open(fixture_file('contigs.fasta'), 'r')

    # TODO: testing with files?
    raise NotImplementedError("This test hasn't been implemented yet!")
